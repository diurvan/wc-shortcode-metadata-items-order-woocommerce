# WC Shortcode MetaData Items Order WooCommerce

Plugin para imprimir a modo de tabla, toda la información insertada como Metadata de cada fila de la orden de WooCommerce.
Este plugin es dependiente de los siguientes plugins: WooCommerce, WooCommerce JetPack, Extensión PDF Invoice, Woocommerce Custom Product Addons.
