<?php
/**
 * Plugin Name: WC Shortcode Metadata Items Order
 * Plugin URI:  https://gitlab.com/diurvan/wc-shortcode-metadata-items-order-woocommerce
 * Description: Plugin to print as a table, all the information inserted as Metadata of each row of the WooCommerce order. This plugin is dependent on the following plugins: WooCommerce, WooCommerce JetPack, PDF Invoice Extension, Woocommerce Custom Product Addons.
 * Author:      diurvan
 * Author URI:  https://diurvanconsultores.com
 * Version: 1.0.1
 * Requires at least: 5.5
 * Tested up to: 5.5
 * Text Domain: diu-wc-shortcode-metadata
 * Domain Path: /languages/
 */

add_shortcode('diu-wc-shortcode-metadata', 'diu_function_wcshortcode');
function diu_function_wcshortcode(){
    $html = '';    
    $order = wc_get_order( do_shortcode('[wcj_order_id]') );
    foreach( $order->get_items() as $item ){
        $html .= '<h3>'.$item['name'].'</h3>';
        $item_meta_data = $item->get_meta_data();
        $html .= '<table>';
        foreach ($item_meta_data as $k => $item) {
            if (! ( substr( $item->key, 0, 1 ) === "_")) {
                $html .='<tr>';
                $html .= '<td>'.$item->key .'</td><td>'.$item->value.'<td/>';
                $html .='</tr>';
            }
        }
        $html .='</table>';
        $html .='<br/>';
    }
    return $html;
}